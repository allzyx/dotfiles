#         ___    __    __ _______  ___  __
#        /   |  / /   / //__  /\ \/ / |/ /
#       / /| | / /   / /   / /  \  /|   /
#      / ___ |/ /___/ /___/ /__ / //   |
#     /_/  |_/_____/_____/____//_//_/|_|
#       __               __                        _____      
#      / /_  ____ ______/ /_     _________  ____  / __(_)___ _
#     / __ \/ __ `/ ___/ __ \   / ___/ __ \/ __ \/ /_/ / __ `/
#    / /_/ / /_/ (__  ) / / /  / /__/ /_/ / / / / __/ / /_/ / 
#   /_.___/\__,_/____/_/ /_/   \___/\____/_/ /_/_/ /_/\__, /  
#                                                 /____/   
#
#   Stole everything I liked from every config I found.



##############################################
###              DO NOT TOUCH              ###
##############################################

[[ $- != *i* ]] && return     # If not running interactively, don't do anything
# alias ls='ls --color=auto'  # ls colors commented out as I'm using exa
# PS1='[\u@\h \W]\$ '         # Prompt is commented out if using starship prompt



###################
###   EXPORTS   ###
###################

export TERM="xterm-256color"                      # getting proper colors
export HISTCONTROL=ignoredups:erasedups           # no duplicate entries
export EDITOR="nvim"
export VISUAL="nvim"
export PATH=$HOME/.local/share/vscode/bin:$PATH   # path to vscode bin



####################
###   MANPAGER   ###
####################

export MANPAGER="sh -c 'col -bx | bat -l man -p'" # bat as manpager
# export MANPAGER="nvim -c 'set ft=man' -"        # nvim as manpager


##############################
###   TITLE OF TERMINALS   ###
##############################

case ${TERM} in
  xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|alacritty|st|konsole*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
        ;;
  screen*)
    PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
    ;;
esac



##############################
###   ARCHIVE EXTRACTION   ###
##############################

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
function extract {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    echo "       extract <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
 else
    for n in "$@"
    do
      if [ -f "$n" ] ; then
          case "${n%,}" in
            *.cbt|*.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar)
                         tar xvf "$n"       ;;
            *.lzma)      unlzma ./"$n"      ;;
            *.bz2)       bunzip2 ./"$n"     ;;
            *.cbr|*.rar)       unrar x -ad ./"$n" ;;
            *.gz)        gunzip ./"$n"      ;;
            *.cbz|*.epub|*.zip)       unzip ./"$n"       ;;
            *.z)         uncompress ./"$n"  ;;
            *.7z|*.arj|*.cab|*.cb7|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.pkg|*.rpm|*.udf|*.wim|*.xar)
                         7z x ./"$n"        ;;
            *.xz)        unxz ./"$n"        ;;
            *.exe)       cabextract ./"$n"  ;;
            *.cpio)      cpio -id < ./"$n"  ;;
            *.cba|*.ace)      unace x ./"$n"      ;;
            *)
                         echo "extract: '$n' - unknown archive method"
                         return 1
                         ;;
          esac
      else
          echo "'$n' - file does not exist"
          return 1
      fi
    done
fi
}
IFS=$SAVEIFS


# Compilation

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
function run {
  if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: compile <path/file_name>.<c|cpp|py|java|go>"
    echo "       compile <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
  else
    for n in "$@"
    do
      if [ -f "$n" ]; then
        fullname="$n"
        filename="${fullname%.*}" 
        case "${n%,}" in  
          *.c)
            gcc --debug "$n" -o "$filename"
            ./"$filename" ;;
          *.cpp)
            g++ --debug "$n" -o "$filename" 
            ./"$filename" ;;
          *) 
            echo "unkown file type"
            return 1 ;;
        esac
      else
        echo "file does not exist"
        return 1
      fi
    done
  fi
}
IFS=$SAVEIFS



# Gaps
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
function gaps {
  hyprland_conf="/home/allzyx/.config/hypr/hyprland.conf"

  if grep -wq "border_size = 2" ${hyprland_conf}; then
    sed -i 's/gaps_in = 5/gaps_in = 0/' ${hyprland_conf}
    sed -i 's/gaps_out = 10/gaps_out = 0/' ${hyprland_conf}
    sed -i 's/border_size = 2/border_size = 0/' ${hyprland_conf}
    sed -i 's/rounding = 10/rounding = 0/' ${hyprland_conf}
  else
    sed -i 's/gaps_in = 0/gaps_in = 5/' ${hyprland_conf}
    sed -i 's/gaps_out = 0/gaps_out = 10/' ${hyprland_conf}
    sed -i 's/border_size = 0/border_size = 2/' ${hyprland_conf}
    sed -i 's/rounding = 0/rounding = 10/' ${hyprland_conf}
  fi
}
IFS=$SAVEIFS


###################
###   ALIASES   ###
###################

# root privileges
alias doas="doas --"

# power
alias shutdown="shutdown -a now"

# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# better ls
alias ls='exa -a --color=always --group-directories-first' 
alias ll='exa -la --color=always --group-directories-first'  
alias lt='exa -aT --color=always --group-directories-first' 
alias l.='exa -a | egrep "^\."'

# apps
alias vim='nvim'
alias fox='firefox'
alias wolf='librewolf'
alias qute='qutebrowser'
alias fm='vifm'

# pacman and yay
alias syu='sudo pacman -Syu'                     # update only standard pkgs
alias syyu='sudo pacman -Syyu'                   # Refresh pkglist & update standard pkgs
alias install='sudo pacman -S '                  # Install pkg
alias search='sudo pacman -Ss '                  # Search for pkg
alias yaysua='yay -Sua --noconfirm'              # update only AUR pkgs (yay)
alias yaysyu='yay -Syu --noconfirm'              # update standard pkgs and AUR pkgs (yay)
alias parsua='paru -Sua --noconfirm'             # update only AUR pkgs (paru)
alias parsyu='paru -Syu --noconfirm'             # update standard pkgs and AUR pkgs (paru)
alias unlock='sudo rm /var/lib/pacman/db.lck'    # remove pacman lock
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)' # remove orphaned packages

# get fastest mirrors
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# Merge Xresources
alias merge='xrdb -merge ~/.Xresources'

# git
alias add='git add .'
alias commit='git commit -m "some fixes"'
alias push='git push origin'
alias pull='git pull origin'
alias clone='git clone'
alias addup='git add -u'
alias branch='git branch'
alias checkout='git checkout'
alias fetch='git fetch'
alias stat='git status'
alias tag='git tag'
alias newtag='git tag -a'


# bluetooth
alias bluezon='sudo systemctl start bluetooth.service'
alias bluezoff='sudo systemctl stop bluetooth.service'
alias airdots='~/dotfiles/bluetooth/airdots'

# wifi
alias wifion='sudo nmcli networking on'
alias wifioff='sudo nmcli networking off'
alias wifihome='~/dotfiles/wifi/wifi-home'

# dotfiles
alias clonedotfiles='git clone https://gitlab.com/allzyx/dotfiles.git'
alias setconfigs="curl -LO https://gitlab.com/allzyx/dotfiles/-/raw/main/set_configs.sh && sh set_configs.sh"

# passwords
alias pw="pass show -c"

# the terminal rickroll
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'


#########################
###   BASH INSULTER   ###
#########################

if [ -f /etc/bash.command-not-found ]; then
    . /etc/bash.command-not-found
fi



#######################################
###   SETTING THE STARSHIP PROMPT   ###
#######################################

eval "$(starship init bash)"

eval "$(starship init bash)"
