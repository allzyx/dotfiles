-----------------------------    MASON SETTINGS    -----------------------------

local status_mason, mason = pcall(require, "mason")
if not status_mason then
  return
end

mason.setup({
  ui = {
    icons = {
      package_installed = "✓",
      package_pending = "➜",
      package_uninstalled = "✗",
    },
    border = "rounded",
    width = 0.5,
    height = 0.6,
    keymaps = {
      toggle_package_expand = "<CR>",
      install_package = "i",
      update_package = "u",
      check_package_version = "c",
      update_all_packages = "U",
      check_outdated_packages = "C",
      uninstall_package = "X",
      cancel_installation = "<C-c>",
      apply_language_filter = "<C-f>",
    },
  },
})

---------------------------    MASON-LSP SETTINGS    ---------------------------

local status_mason_lsp, mason_lspconfig = pcall(require, "mason-lspconfig")
if not status_mason_lsp then
  return
end

mason_lspconfig.setup({
  ensure_installed = {
    "lua_ls",
    "jsonls",
    "clangd",
    "pyright",
    "jdtls",
    "bashls",
    -- "vimls",
    -- "html",
    -- "cssls",
  },
})
