---------------------------    CATPPUCCIN SETTINGS    --------------------------

local status_catppuccin, catppuccin = pcall(require, "catppuccin")
if not status_catppuccin then
	return
end

catppuccin.setup({
	flavour = "macchiato", -- latte, frappe, macchiato, mocha
	background = { -- :h background
		light = "latte",
		dark = "mocha",
	},
	transparent_background = true,
	show_end_of_buffer = false, -- shows the '~' characters
	term_colors = false,
	dim_inactive = {
		enabled = false,
		shade = "dark",
		percentage = 0.05,
	},
	no_italic = true, -- Force no italic
	no_bold = false, -- Force no bold
	no_underline = false, -- Force no underline
	styles = { -- Handles the styles of general hi groups
		comments = { "italic" }, -- Change the style of comments
		conditionals = { "italic" },
		loops = {},
		functions = {},
		keywords = {},
		strings = {},
		variables = {},
		numbers = {},
		booleans = {},
		properties = {},
		types = {},
		operators = {},
	},
	color_overrides = {},
	custom_highlights = {},
	integrations = {
		treesitter = true,
		nvimtree = true,
		gitsigns = true,
		bufferline = true,
		--lualine = true,
		telescope = true,
		--telescope = {
		-- enabled = true,
		-- style = "nvchad"
		--},
		cmp = true,
		notify = false,
		mini = false,
	},
})
