----------------------------    VIMWIKI SETTINGS    ----------------------------

vim.g.vimwiki_folding = ""

vim.g.vimwiki_list = {
	{
		path = "~/.local/wiki",
		path_html = "~/.local/wiki_html",
		template_path = "~/.local/wiki_html/templates",
		template_default = "def_template",
		template_ext = ".html",
		auto_toc = 1,
		syntax = "markdown",
		ext = ".md",
	},
}
