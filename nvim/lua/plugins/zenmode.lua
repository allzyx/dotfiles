----------------------------    ZEN-MODE SETTINGS    ---------------------------

local status_zen_mode, zen_mode = pcall(require, "zen-mode")
if not status_zen_mode then
	return
end

zen_mode.setup({
	window = {
		backdrop = 1, -- shade the backdrop of the zen window
		width = 80, -- width of zen window
		height = 1, -- hight of zen window

		options = {
			signcolumn = "no", -- disable signcolumn
			number = false, -- disable number column
			relativenumber = false, -- disable relative numbers
			cursorline = true, -- disable cursorline
			cursorcolumn = false, -- disable cursor column
			-- foldcolumn = "0",                      -- disable fold column
			-- list = false,                          -- disable whitespace characters
		},
	},

	plugins = {
		options = {
			enabled = true, -- enable plugin
			ruler = false, -- disables the ruler text in the cmd line area
			showcmd = false, -- disables the command in the last line of the screen
		},

		gitsigns = { enabled = true }, -- disables git signs
		--tmux = { enabled = false },          -- disables the tmux statusline
		--twilight = { enabled = true },       -- start Twilight when zen mode opens

		-- alacritty = {
		-- 	enabled = true,
		-- 	font = "20", -- Increase font size in zen-mode
		-- },
	},

	on_open = function(win) end, -- Execute when enter zen mode
	on_close = function() end, -- Execute when exit zen mode
})
