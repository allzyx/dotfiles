----------------------------    NEODEV SETTINGS    -----------------------------

local status_neodev, neodev = pcall(require, "neodev")
if not status_neodev then
	return
end

neodev.setup({
	library = { plugins = { "nvim-dap-ui" }, types = true },
})
