--------------------------    TREESITTER SETTINGS    ---------------------------

local status_treesitter, treesitter = pcall(require, "nvim-treesitter.configs")
if not status_treesitter then
	return
end
treesitter.setup({

	ensure_installed = {
		"lua",
		"vim",
		"vimdoc",
		"json",
		"yaml",
		"toml",
		"c",
		"cpp",
		"html",
		"css",
	},

	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false,
	},

	sync_install = false,
	auto_install = false,

	-----------------------    RAINDOW BRACKETS SETTINGS    ----------------------

	rainbow = {
		enable = true,
		-- disable = { "jsx", "cpp" }, languages you want to disable the plugin for
		extended_mode = true, -- Also highlight non-bracket delimiters like html tags
		max_file_lines = nil, -- Do not enable for files with more than n lines, int
		-- colors = {}, -- table of hex strings
	},
})

---------------------------    COLORIZER SETTINGS    ---------------------------

local status_colorizer, colorizer = pcall(require, "colorizer")
if not status_colorizer then
	return
end

colorizer.setup()
