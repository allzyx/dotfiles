----------------------------    NULL-LS SETTINGS    ----------------------------

local status_null_ls, null_ls = pcall(require, "null-ls")
if not status_null_ls then
  return
end

local formatting = null_ls.builtins.formatting
local diagnostics = null_ls.builtins.diagnostics

local sources = {

  -- Linters
  diagnostics.luacheck,  -- lua
  diagnostics.jsonlint,  -- json
  diagnostics.cpplint,   -- c, c++
  diagnostics.mypy,      -- python
  diagnostics.ruff,      -- python
  diagnostics.shellcheck, -- bash

  -- Formatters
  formatting.stylua,      -- lua
  formatting.clang_format, -- c, c++
  formatting.black,       -- python
  formatting.beautysh,    -- bash
  formatting.prettierd, --[[ angular, css, flow, graphql, html,
	                           json, jsx, javascript, less, markdown,
	                           scss, typescript, vue, yaml ]]
}

local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
null_ls.setup({
  on_attach = function(client, bufnr)
    if client.supports_method("textDocument/formatting") then
      vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
      vim.api.nvim_create_autocmd("BufWritePre", {
        group = augroup,
        buffer = bufnr,
        callback = function()
          vim.lsp.buf.format({ async = false })
        end,
      })
    end
  end,

  sources = sources,
})

-------------------------    MASON-NULL-LS SETTINGS    -------------------------

local status_mason_null_ls, mason_null_ls = pcall(require, "mason-null-ls")
if not status_mason_null_ls then
  return
end

mason_null_ls.setup({
  ensure_installed = {
    -- Linters
    "luacheck",
    "jsonlint",
    "cpplint",
    "mypy",
    "ruff",
    "shellcheck",

    -- Formatters
    "stylua",
    "clang-format",
    "black",
    "prettierd",
    "beautysh",

    -- Debuggers
    "codelldb",
    "debugpy",
    "bash-debug-adapter",
  },
  automatic_installation = true,
})
