--------------------------    BUFFERLINE SETTINGS    ---------------------------

local status_bufferline, bufferline = pcall(require, "bufferline")
if not status_bufferline then
	return
end

bufferline.setup({
	options = {
		mode = "buffers", -- "tabs" to only show tabpages instead
		themable = true, -- allows highlight groups to be overriden
		numbers = "none", -- "ordinal" "buffer_id" "both" function
		indicator = {
			--icon = '▎',   -- this should be omitted if indicator style is not 'icon'
			style = "none", -- 'icon' 'underline' 'none'
		},
		buffer_close_icon = "󰅖",
		modified_icon = "●",
		close_icon = "",
		left_trunc_marker = "",
		right_trunc_marker = "",
		max_name_length = 18,
		max_prefix_length = 15, -- prefix used when a buffer is de-duplicated
		truncate_names = true, -- whether or not tab names should be truncated
		tab_size = 18,
		diagnostics = "nvim_lsp", -- false | "nvim_lsp" | "coc"
		diagnostics_update_in_insert = false,
		offsets = {
			{
				filetype = "NvimTree",
				text = "File Explorer",
				text_align = "center",
				separator = false,
			},
		},
		color_icons = true, -- colored filetype icons or not
		show_buffer_icons = true, -- disable filetype icons for buffers
		show_buffer_close_icons = false,
		show_close_icon = false,
		show_tab_indicators = false,
		show_duplicate_prefix = true, -- show duplicate buffer prefix
		persist_buffer_sort = true, -- custom sorted buffers should persist
		move_wraps_at_ends = false, -- move command "wraps" at ends
		separator_style = { "", "" }, -- "slant" "slope" "thick" "thin"
		enforce_regular_tabs = false,
		always_show_bufferline = true,
		sort_by = "insert_at_end",
	},

	-- highlights = {
	--   fill = {
	--       -- fg = '<colour-value-here>',
	--       bg = '',
	--   },
	--   background = {
	--       fg = '<colour-value-here>',
	--       bg = '<colour-value-here>',
	--   },
	-- },
})
