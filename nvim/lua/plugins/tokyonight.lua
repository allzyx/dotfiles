--------------------------    TOKYONIGHT SETTINGS    ---------------------------

local status_tokyonight, tokyonight = pcall(require, "tokyonight")
if not status_tokyonight then
  return
end

tokyonight.setup({
  style = "storm", -- `storm`, `moon`, `night`, `day`
  dark_style = "night",
  light_style = "day",
  transparent = false,
  terminal_colors = true,
  styles = {
    comments = { italic = true },
    keywords = { italic = true },
    functions = {},
    variables = {},

    sidebars = "transparent", -- "dark", "transparent" or "normal"
    floats = "transparent", --"dark", "transparent" or "normal"
  },
  -- Set a darker background on sidebar-like windows. ["terminal", "packer"]
  sidebars = {},
  day_brightness = 0.3,
  hide_inactive_statusline = true,
  dim_inactive = false,
  lualine_bold = false,

  on_colors = function(colors) end,
  on_highlights = function(highlights, colors) end,
})
