---------------------------    AUTOPAIRS SETTINGS    ---------------------------

local status_autopairs, autopairs = pcall(require, "nvim-autopairs")
if not status_autopairs then
	return
end

autopairs.setup({
	disable_filetype = { "TelescopePrompt", "vim" },
})

----------------------------    AUTOTAG SETTINGS    ----------------------------

local status_autotag, autotag = pcall(require, "nvim-ts-autotag")
if not status_autotag then
	return
end

autotag.setup()
