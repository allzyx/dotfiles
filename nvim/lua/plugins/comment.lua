----------------------------    COMMENT SETTINGS    ----------------------------

local status_comment, comment = pcall(require, "Comment")
if not status_comment then
	return
end

comment.setup({
	padding = true, -- Add a space b/w comment and the line
	sticky = true, -- Whether the cursor should stay at its position
	ignore = nil, --Lines to be ignored while (un)comment

	-- Mappings
	toggler = {
		line = "cl", -- Toggle line comment
		block = "cb", -- Toggle block comment
	},
	opleader = {
		line = "cl", -- Toggle line comment
		block = "cb", -- Toggle block comment
	},

	-- Extra mappings
	extra = {
		above = "ca", -- Add comment on the line above
		below = "cb", -- Add comment on the line below
		eol = "ce", -- Add comment at the end of line
	},

	mappings = {
		basic = true,
		extra = true,
	},

	pre_hook = nil, -- Function to call before (un)comment
	post_hook = nil, -- Function to call after (un)comment
})
