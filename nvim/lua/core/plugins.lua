----------------------------------    LAZY    ----------------------------------

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim" -- Plugin manager
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)
require("lazy").setup({

	--------------------------------    THEMES    --------------------------------

	{
		"folke/tokyonight.nvim", -- Best theme ever
		lazy = false,
		priority = 1000,
		config = function()
			require("plugins.tokyonight")
			vim.cmd.colorscheme("tokyonight")
		end,
	},

	-- {
	-- 	"catppuccin/nvim", -- Theme with pastel colors
	-- 	name = "catppuccin",
	-- 	priority = 1000,
	-- 	lazy = false,
	-- 	config = function()
	-- 		require("plugins.catppuccin")
	-- 		vim.cmd.colorscheme("catppuccin")
	-- 	end,
	-- },

	-- {
	-- 	"nordtheme/vim",
	-- 	priority = 1000,
	-- 	lazy = false,
	-- 	config = function()
	-- 		require("plugins.nord")
	-- 		vim.cmd.colorscheme("nord")
	-- 	end,
	-- },

	-- {
	-- 	"sainnhe/everforest",
	-- 	priority = 1000,
	-- 	lazy = false,
	-- 	config = function()
	-- 		require("plugins.everforest")
	-- 		vim.cmd.colorscheme("everforest")
	-- 	end,
	-- },

	-- {
	-- 	"ellisonleao/gruvbox.nvim",
	-- 	priority = 1000,
	-- 	lazy = false,
	-- 	config = function()
	-- 		require("plugins.gruvbox")
	-- 		vim.cmd.colorscheme("gruvbox")
	-- 	end,
	-- },

	------------------------------    INTERFACE    -------------------------------

	{
		"nvim-tree/nvim-web-devicons", -- Icons
	},

	{
		"akinsho/bufferline.nvim", -- Shows opend buffers
		event = "BufEnter",
		version = "*",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		config = function()
			require("plugins.bufferline")
		end,
	},

	{
		"nvim-lualine/lualine.nvim", -- Replace for default status line
		event = "BufEnter",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		config = function()
			require("plugins.lualine")
		end,
	},

	{
		"glepnir/dashboard-nvim", -- Startup screen
		event = "VimEnter",
		dependencies = { { "nvim-tree/nvim-web-devicons" } },
		config = function()
			require("plugins.dashboard")
		end,
	},

	{
		"folke/zen-mode.nvim", -- Zen mode
		event = "VeryLazy",
		config = function()
			require("plugins.zenmode")
		end,
	},

	{
		"folke/noice.nvim",
		event = "VeryLazy",
		dependencies = {
			"MunifTanjim/nui.nvim",
			-- OPTIONAL:
			--   `nvim-notify` is only needed, if you want to use the notification view.
			--   If not available, we use `mini` as the fallback
			-- "rcarriga/nvim-notify",
		},
		config = function()
			require("plugins.noice")
		end,
	},

	----------------------------    WORK WITH FILES    ---------------------------

	{
		"nvim-tree/nvim-tree.lua", -- File explorer
		event = "VeryLazy",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		config = function()
			require("plugins.nvimtree")
		end,
	},

	{
		"mbbill/undotree",
		event = "VeryLazy",
	},

	{
		"nvim-telescope/telescope.nvim", -- Fuzzy searcher
		event = "VeryLazy",
		tag = "0.1.2",
		dependencies = { "nvim-lua/plenary.nvim" },
		config = function()
			require("plugins.telescope")
		end,
	},

	{
		"ThePrimeagen/harpoon",
		event = "VeryLazy",
		config = function()
			require("plugins.harpoon")
		end,
	},

	-------------------------    SYNTAX HIGHLIGHTING    --------------------------

	{
		"nvim-treesitter/nvim-treesitter", -- Syntax highlighter
		event = "VeryLazy",
		config = function()
			require("plugins.syntax_highlight")
		end,
	},

	{
		"p00f/nvim-ts-rainbow", -- Colorized brackets
		event = "VeryLazy",
	},

	{
		"norcalli/nvim-colorizer.lua", -- Shows colors like #AAF5FF
		event = "VeryLazy",
	},

	---------------------------    EASY TEXT EDITING    --------------------------

	{
		"numToStr/Comment.nvim", -- Comment / uncomment with binds
		event = "VeryLazy",
		config = function()
			require("plugins.comment")
		end,
	},

	{
		"windwp/nvim-autopairs", -- Adds closing brackets and quotes {[("")]}
		event = "VeryLazy",
		config = function()
			require("plugins.autopairs")
		end,
	},

	{
		"windwp/nvim-ts-autotag", -- Adds closing tags
		event = "VeryLazy",
	},

	{
		"kylechui/nvim-surround",
		version = "*",
		event = "VeryLazy",
		config = function()
			require("nvim-surround").setup({})
		end,
	},

	{
		"folke/flash.nvim",
		event = "VeryLazy",
		opts = {},
		keys = {
			{
				"s",
				mode = { "n", "o", "x" },
				function()
					require("flash").jump()
				end,
				desc = "Flash",
			},
			{
				"S",
				mode = { "n", "o", "x" },
				function()
					require("flash").treesitter()
				end,
				desc = "Flash Treesitter",
			},
			{
				"r",
				mode = "o",
				function()
					require("flash").remote()
				end,
				desc = "Remote Flash",
			},
			{
				"R",
				mode = { "o", "x" },
				function()
					require("flash").treesitter_search()
				end,
				desc = "Treesitter Search",
			},
			{
				"<c-s>",
				mode = { "c" },
				function()
					require("flash").toggle()
				end,
				desc = "Toggle Flash Search",
			},
		},
	},

	-------------------------------    TERMINAL    -------------------------------

	{
		"akinsho/toggleterm.nvim",
		event = "VeryLazy",
		config = function()
			require("plugins.toggleterm")
		end,
	},

	---------------------------------    GIT    ----------------------------------

	{
		"lewis6991/gitsigns.nvim",
		event = "VeryLazy",
		config = function()
			require("plugins.gitsigns")
		end,
	},

	{
		"tpope/vim-fugitive",
		event = "VeryLazy",
	},

	---------------------------------    LSP    ----------------------------------

	{
		"williamboman/mason.nvim", -- Package manager for LSP's, linters etc
		event = "VeryLazy",
		config = function()
			require("plugins.mason")
		end,
	},

	{
		"williamboman/mason-lspconfig.nvim", -- For functions like ensure_installed
		event = "VeryLazy",
	},

	{
		"neovim/nvim-lspconfig", -- LSP configurations
		event = "VeryLazy",
		config = function()
			require("plugins.lspconfig")
		end,
	},

	------------------------------    COMPLETION    ------------------------------

	{
		"hrsh7th/nvim-cmp",
		event = "InsertEnter",
		config = function()
			require("plugins.nvimcmp")
		end,
	},

	{
		"hrsh7th/cmp-nvim-lsp",
		event = "VeryLazy",
	},

	{
		"hrsh7th/cmp-nvim-lua",
		event = "VeryLazy",
	},

	{
		"hrsh7th/cmp-path",
		after = "nvim-cmp",
	},

	{
		"hrsh7th/cmp-buffer",
		after = "nvim-cmp",
	},

	{
		"hrsh7th/cmp-cmdline",
		after = "nvim-cmp",
	},

	-------------------------------    SNIPPETS    -------------------------------

	{
		"L3MON4D3/LuaSnip",
		event = "VeryLazy",
	},

	{
		"rafamadriz/friendly-snippets",
		event = "VeryLazy",
	},

	--------------------------------    DEBUG    ---------------------------------

	{
		"rcarriga/nvim-dap-ui",
		event = "VeryLazy",
		dependencies = {
			"mfussenegger/nvim-dap",
			"nvim-neotest/nvim-nio",
		},
		config = function()
			local dap = require("dap")
			local dapui = require("dapui")
			dapui.setup()
			dap.listeners.after.event_initialized["dapui_config"] = function()
				dapui.open()
			end
			dap.listeners.before.event_terminated["dapui_config"] = function()
				dapui.close()
			end
			dap.listeners.before.event_exited["dapui_config"] = function()
				dapui.close()
			end
		end,
	},

	{
		"jay-babu/mason-nvim-dap.nvim",
		event = "VeryLazy",
		dependencies = { "williamboman/mason.nvim", "mfussenegger/nvim-dap" },
		opts = { handlers = {} },
	},

	{
		"folke/neodev.nvim",
		event = "VeryLazy",
		config = function()
			require("plugins.neodev")
		end,
	},

	------------------------    LINT / FORMAT / DEBUG    -------------------------

	{
		"jose-elias-alvarez/null-ls.nvim",
		event = "VeryLazy",
		config = function()
			require("plugins.null-ls")
		end,
	},

	{
		"jay-babu/mason-null-ls.nvim",
		event = { "BufReadPre", "BufNewFile" },
		dependencies = {
			"williamboman/mason.nvim",
			"jose-elias-alvarez/null-ls.nvim",
		},
	},

	----------------------------------    AI    ----------------------------------

	{
		"Exafunction/codeium.vim",
		event = "BufEnter",
		config = function()
			require("plugins.codeium")
		end,
	},

	-----------------------------    NOTE TAKING    ------------------------------

	{
		"vimwiki/vimwiki",
		config = function()
			require("plugins.vimwiki")
		end,
		event = "BufEnter *.md",
		keys = {
			"<leader>ww",
			"<leader>wi",
			"<leader>w<leader>w",
			"<leader>w<leader>y",
			"<leader>w<leader>m",
		},
	},

	{
		"iamcco/markdown-preview.nvim",
		after = "vimwiki",
		config = function()
			vim.fn["mkdp#util#install"]()
		end,
	},
})
