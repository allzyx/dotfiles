local opts = { noremap = true, silent = true } -- ??
local term_opts = { silent = true }            -- ??

-- Shorten function name
local map = vim.api.nvim_set_keymap

------------------------------    LEADER KEY    --------------------------------

vim.g.mapleader = " "
vim.g.maplocalleader = " "

--------------------------------    INSERT    ----------------------------------

-- Exit insert mode
map("i", "jj", "<ESC>", opts)
map("i", "jk", "<ESC>", opts)
map("i", "kj", "<ESC>", opts)
map("i", "kk", "<ESC>", opts)

--------------------------------    NORMAL    ----------------------------------

-- Basic
map("n", "<leader>q", ":q<CR>", opts)
map("n", "<leader>c", ":bd<CR>", opts)
map("n", "<leader>s", ":w<CR>", opts)

-- Buffer navigation
map("n", "<S-l>", ":bnext<CR>", opts)
map("n", "<S-h>", ":bprevious<CR>", opts)

-- New tab
map("n", "te", ":tabedit<CR>", opts)

-- Split window
map("n", "sv", ":vsplit<CR>", opts)
map("n", "sh", ":split<CR>", opts)

-- Window navigation
map("n", "<C-l>", "<C-w>l", opts)
map("n", "<C-h>", "<C-w>h", opts)
map("n", "<C-j>", "<C-w>j", opts)
map("n", "<C-k>", "<C-w>k", opts)

-- Window resize
map("n", "<Down>", ":resize +2<CR>", opts)
map("n", "<Up>", ":resize -2<CR>", opts)
map("n", "<Left>", ":vertical resize -2<CR>", opts)
map("n", "<Right>", ":vertical resize +2<CR>", opts)

-- Open things
map("n", "<leader>e", ":NvimTreeToggle<CR>", opts)
map("n", "<leader>u", ":UndotreeToggle<CR>", opts)
map("n", "<leader>pl", ":Lazy<CR>", opts)
map("n", "<leader>pm", ":Mason<CR>", opts)

-- Telescope
map("n", "<leader>ff", ":Telescope find_files<CR>", opts)
map("n", "<leader>fw", ":Telescope live_grep<CR>", opts)
map("n", "<leader>fc", ":Telescope current_buffer_fuzzy_find<CR>", opts)
map("n", "<leader>fb", ":Telescope buffers<CR>", opts)
map("n", "<leader>fs", ":Telescope git_status<CR>", opts)
map("n", "<leader>fh", ":Telescope help_tags<CR>", opts)
map("n", "<leader>fv", ":Telescope harpoon marks<CR>", opts)

-- Terminal
map("n", "<leader>t", ":ToggleTerm border=normal direction=float<CR>", opts)

-- Markdown Preview
-- map("n", "<leader>p", ":MarkdownPreview<CR>", opts)
-- map("n", "<leader>ps", ":MarkdownPreviewStop<CR>", opts)

-- Git
map("n", "<leader>gs", ":Git<CR>", opts)
map("n", "<leader>b", ":Gitsigns blame_line<CR>", opts)
map("n", "<leader>gb", ":Gitsigns toggle_current_line_blame<CR>", opts)
--map("n", "<leader>gs", ":Gitsigns toggle_signs<CR>", opts)
--map("n", "<leader>gn", ":Gitsigns toggle_numhl<CR>", opts)
--map("n", "<leader>gl", ":Gitsigns toggle_linehl<CR>", opts)
--map("n", "<leader>gd", ":Gitsigns toggle_word_diff<CR>", opts)

-- Zen mode
map("n", "zz", ":ZenMode<CR>", opts)

-- Increment / Decrement
map("n", "=", "<C-a>", opts)
map("n", "-", "<C-x>", opts)

-- Don't yand text deleted with x
map("n", "x", '"_x', opts)

-- Don't move cursor after J
map("n", "J", "mzJ`z", opts)

-- Select all
map("n", "<C-a>", "gg<S-v>G", opts)

-- Turn off search highlight
map("n", "<leader>nh", ":nohlsearch<CR>", opts)

-- Formating
map("n", "ff", ":lua vim.lsp.buf.format()<CR>", opts)

-- Debugging
map("n", "<leader>db", ":DapToggleBreakpoint<CR>", opts)
map("n", "<leader>dr", ":DapContinue<CR>", opts)
map("n", "<leader>di", ":DapStepInto<CR>", opts)
map("n", "<leader>do", ":DapStepOver<CR>", opts)
map("n", "<leader>du", ":DapStepOut<CR>", opts)
map("n", "<leader>ds", ":DapTerminate<CR>", opts)
-- map("n", "<leader>dt", ":lua require('dapui').toggle()<CR>", opts)
-- map("n", "<leader>dr", ":lua require('dapui').open({reset = true})<CR>", opts)

-- Toggle line nubmers
map("n", "<leader>nn", ":exec &nu==&rnu? 'se nu!' : 'se rnu!'<CR>", opts)

-- Harpoon
map("n", "<leader>m", ":lua require('harpoon.mark').add_file()<CR>", opts)
map("n", "<leader>h", ":lua require('harpoon.ui').toggle_quick_menu()<CR>", opts)
map("n", ".", ":lua require('harpoon.ui').nav_next()<CR>", opts)
map("n", ",", ":lua require('harpoon.ui').nav_prev()<CR>", opts)

-- Vimwiki
map("n", "<leader>wa", ":VimwikiAll2HTML<CR>", opts)
map("n", "<leader>wb", ":Vimwiki2HTMLBrowse<CR>", opts)
map("n", "<leader>ws", ":VimwikiVSplitLink<CR>", opts)

--------------------------------    VISUAL    ----------------------------------

-- Exit with q
map("v", "q", "<ESC>", opts)

-- Dont yank replaced text
map("v", "p", '"_dP', opts)

-- Tabulation
map("v", ">", ">gv", opts)
map("v", "<", "<gv", opts)

-----------------------------    VISUAL BLOCK    -------------------------------

-- Exit with q
map("x", "q", "<ESC>", opts)

-- Move text
map("x", "K", ":move '<-2<CR>gv=gv", opts)
map("x", "J", ":move '>+1<CR>gv=gv", opts)

-------------------------------    TERMINAL    ---------------------------------

-- Terminal mode navigation
map("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
map("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
map("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
map("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)

--------------------------------    COMMAND    ---------------------------------

-- Command mode navigation
map("c", "<C-j>", 'pumvisible()?"\\<C-n>":"\\<C-j>"', { expr = true, noremap = true })
map("c", "<C-k>", 'pumvisible()?"\\<C-p>":"\\<C-k>"', { expr = true, noremap = true })
