vim.cmd("set whichwrap+=<,>,[,],h,l") -- When line ends go to the next one

vim.opt.shortmess:append("c") -- ??

local options = {

	---------------------------    BACKUP AND SWAP    ----------------------------

	backup = false, -- Disable backups
	writebackup = false, -- Allow to edit already opened files
	swapfile = false, -- Disable swaps

	-------------------------------    GENERAL    --------------------------------

	clipboard = "unnamedplus", -- Force to use system clipboard
	undofile = true, -- Enable persistent undo
	mouse = "a", -- Allow to use mouse
	fileencoding = "utf-8", -- The encoding written to a file
	hidden = true, -- Allows to load multiple buffers in memory
	conceallevel = 0, -- So `` is visible in markdown files
	timeoutlen = 250, -- Time to press map sequence
	updatetime = 300, -- Faster completion ??
	completeopt = { -- Mostly just for cmp ??
		"menuone",
		"noselect",
	},

	-----------------------------    INDETATION    -------------------------------

	smartindent = true, -- Smart indents
	expandtab = true, -- Convert tabs to spaces
	shiftwidth = 2, -- Number of spaces for indentation
	tabstop = 2, -- Insert 2 spaces for a tab
	softtabstop = 2, -- ??

	------------------------------    INTERFACE    -------------------------------

	number = true, -- Lines numbers
	relativenumber = true, -- Relative lines numbers
	numberwidth = 2, -- Width of number column
	colorcolumn = "81", -- Colored column to control line length
	cursorline = true, -- Highlight the current line
	showmode = false, -- Don't show modes (insert,visual)
	cmdheight = 1, -- Width of command line
	showtabline = 0, -- Always show tabline
	pumheight = 10, -- Pop up menu height ??
	signcolumn = "yes", -- Always show the sign column
	termguicolors = true, -- Set term gui colors
	guicursor = "", -- Block cursor for all modes
	-- guifont = "monospace:h17",             -- Font for graphical neovim apps ??

	-------------------------------    BEHAVIOR    -------------------------------

	scrolloff = 5, -- Scroll when X lines below cursor
	sidescrolloff = 5, -- Same but for left/right
	wrap = false, -- Display lines as one long line

	--------------------------------    SEARCH    --------------------------------

	hlsearch = true, -- ??
	ignorecase = true, -- Ignore case in search patterns
	smartcase = true, -- Smart case

	--------------------------------    SPLITS    --------------------------------

	splitbelow = true, -- Horizontal splits go below
	splitright = true, -- Vertical splits go to the right
}

-- Sets options from list above
for option, value in pairs(options) do
	vim.opt[option] = value
end
