------------------------------    SOURCE FILES    ------------------------------

require("core.options") -- Vim options
require("core.mappings") -- All mappings
require("core.plugins") -- Enables plugins

-- Sourse vimwiki settings here because it don't work if sourced in plugins.lua
require("plugins.vimwiki")
