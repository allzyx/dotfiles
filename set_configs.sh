#! /bin/bash


# Use following command to download script and execute it
# curl -LO https://gitlab.com/allzyx/dotfiles/-/raw/main/set_configs.sh && sh set_configs.sh

#------------------------------    VARIABLES    ------------------------------#

username="allzyx"
email="allzyx@proton.me"

dotfiles_repo="git@gitlab.com:allzyx/dotfiles.git"
dotfiles_dir="$HOME/.config/dotfiles"

wallpapers_repo="git@gitlab.com:allzyx/wallpapers.git"
wallpapers_dir="$HOME/.local/wallpapers/tokyonight"
wallpapers="$wallpapers_dir/outer_space.png"

src_dir="$HOME/.local/src"

# wiki_repo="git@gitlab.com:allzyx/wiki.git"
# wiki_dir="$HOME/.local/wiki"

keys_repo="git@gitlab.com:allzyx/gpg-keys.git"
keys_dir="$HOME/.gpg-keys"

passwords_repo="git@gitlab.com:allzyx/password-store.git"
passwords_dir="$HOME/.password-store"

ly_bg_color="24283B" # Tokyonight bg #24283B
ly_fg_color="A9B1D6" # Tokyonight fg #A9B1D6
# ly_bg_color="24283B" # Catppuccin bg #1E1E2E
# ly_fg_color="CDD6F4" # Catppuccin fg #CDD6F4
# ly_bg_color="2D353B" # Everforest bg #2D353B
# ly_fg_color="D3C6AA" # Everforest fg #D3C6AA

#-----------------------------    PREPARATION    -----------------------------#

# System update
cd || exit 1; sudo pacman -Syyu

# Install base-devel and git
sudo pacman -S base-devel --needed --noconfirm
sudo pacman -S git --needed --noconfirm

# Make dirictory for sources
mkdir "$src_dir"

# Install yay
if command -v yay &>/dev/null; then
    echo "yay is already installed"
else
    git clone https://aur.archlinux.org/yay.git "$src_dir"/yay
    cd "$src_dir"/yay/ && makepkg -si
fi

# Clone dotfiles repo
rm -rf "$dotfiles_dir"
git clone $dotfiles_repo "$dotfiles_dir"

# Clone wallpapers repo
git clone $wallpapers_repo "$wallpapers_dir"

#----------------------------  INSTALL PACKAGES  -----------------------------#

sudo pacman -S swaylock --needed --noconfirm
sudo pacman -S syncthing --needed --noconfirm
sudo pacman -S firefox --needed --noconfirm
sudo pacman -S zsh alacritty exa bat neofetch htop starship --needed --noconfirm
sudo pacman -S neovim ripgrep gcc ttf-jetbrains-mono-nerd --needed --noconfirm
sudo pacman -S swww ly pass --noconfirm
sudo pacman -S jdk-open-jdk gradle luarocks npm --needed --noconfirm
sudo pacman -S luarocks npm --needed --noconfirm
yay -S catppuccin-cursors-mocha --needed --noconfirm

#-----------------------------    SET CONFIGS    -----------------------------#

# Bash
cp -f "$dotfiles_dir"/bash/.bashrc $HOME/
cp -f "$dotfiles_dir"/bash/.bash_profile $HOME/
cp -f "$dotfiles_dir"/bash/.bash_logout $HOME/

# Zsh
cp -f "$dotfiles_dir"/zsh/.zshrc $HOME/

# Alacritty
cp -rf "$dotfiles_dir"/alacritty ~/.config/

# Tmux
cp -rf "$dotfiles_dir"/tmux ~/.config/
rm -rf ~/.local/src/tmux/
git clone https://github.com/tmux-plugins/tpm ~/.local/src/tmux/plugins/tpm/

# Neovim
rm -rf ~/.config/nvim ~/.local/share/nvim ~/.local/state/nvim
cp -rf "$dotfiles_dir"/nvim ~/.config/nvim

# Btop
cp -rf "$dotfiles_dir"/btop/btop.conf ~/.config/btop/

# Starship
cp -f "$dotfiles_dir"/starship/starship.toml ~/.config/

# Git
git config --global user.name  $username
git config --global user.email $email

# Ly
sudo rm -f /etc/systemd/system/display-manager.service
sudo systemctl enable ly.service
sudo sed -i '/^ExecStartPre=\/usr\/bin\/printf/d' /lib/systemd/system/ly.service
sudo sed -i "/Type=idle/a ExecStartPre=/usr/bin/printf '%%b' '\\\e]P0$ly_bg_color\\\e]P7$ly_fg_color\\\ec' " /lib/systemd/system/ly.service
sudo sed -i 's/^#blank_password = false/blank_password = true/' /etc/ly/config.ini
sudo sed -i 's/^#bigclock = true/bigclock = true/' /etc/ly/config.ini
sudo sed -i 's/^#hide_f1_commands = false/hide_f1_commands = true/' /etc/ly/config.ini
sudo sed -i '/term_reset_cmd/d' /etc/ly/config.ini
sudo sed -i "/# Terminal reset command (tput is faster)/a term_reset_cmd = \/usr\/bin\/tput reset; \/usr\/bin\/printf '%b' '\\\e]P7$ly_fg_color\\\e]P0$ly_bg_color\\\ec' " /etc/ly/config.ini

# Firefox
cp -rf "$dotfiles_dir"/firefox/chrome $HOME/.mozilla/firefox/*.default-release/

# Waybar
cp -rf "$dotfiles_dir"/waybar $HOME/.config/

# Hyprland
cp -rf "$dotfiles_dir"/hypr $HOME/.config/

# Swaylock
cp -rf "$dotfiles_dir"/swaylock/tokyonight $HOME/.config/swaylock

# Wlogout
cp -rf "$dotfiles_dir"/wlogout $HOME/.config/
sudo rm -rf /usr/share/wlogout/icons/
sudo cp -rf "$dotfiles_dir"/wlogout/tokyonight/icons /usr/share/wlogout/icons

# Wofi
cp -rf "$dotfiles_dir"/wofi $HOME/.config/

# Set wallpapers
swww img "$wallpapers"


#---------------------------------    WIKI    --------------------------------#

git clone $wiki_repo "$wiki_dir"

#------------------------------    PASSWORDS    ------------------------------#

# Import gpg keys
rm -rf "$keys_dir"
git clone $keys_repo "$keys_dir"
cd "$keys_dir" && gpg --import private.pgp
key=`gpg -K | grep 9BF6`
FP=$(gpg --list-keys "$key" | head -n2 | tail -n1 | tr -d '[:blank:]')
echo -e "5\ny\n" | gpg --command-fd 0 --edit-key "$FP" trust

# Import passwords
sudo pacman -S pass --noconfirm --needed
rm -rf "$passwords_dir"
git clone $passwords_repo "$passwords_dir"

#-------------------------------    CLEANUP    -------------------------------#

rm -f ~/set_configs.sh
